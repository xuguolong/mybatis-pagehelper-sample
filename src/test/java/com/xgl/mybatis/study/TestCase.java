package com.xgl.mybatis.study;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.xgl.mybatis.study.mapper.UserMapper;
import com.xgl.mybatis.study.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TestCase {

    private SqlSession sqlSession;
    private UserMapper userMapper;


    @Before
    public void before() throws Exception{
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();

        SqlSessionFactory sqlSessionFactory = builder.build(Resources.getResourceAsReader("mybatis-config.xml"));

        sqlSession = sqlSessionFactory.openSession(true);
        userMapper = sqlSession.getMapper(UserMapper.class);

    }


    @Test
    public void after() {
        sqlSession.commit();
        sqlSession.close();
    }




    @Test
    public void run1() throws Exception{

        User param = new User();
        param.setName("user2");
        List<User> result = userMapper.selectUserList(param);
        System.out.println(result);
    }


    /**
     * 完整分页例子
     * 使用PageHelper.startPage的方式
     * @throws Exception
     */
    @Test
    public void run2() throws Exception{

        User param = new User();
        //param.setName("user2");

        int pageNum = 1;
        int pageSize=10;

        System.out.println("第"+pageNum+"页查询");
        PageHelper.startPage(pageNum, pageSize);
        Page<User> page = (Page<User>)userMapper.selectUserList(param);
        for(User user:page.getResult()){
            System.out.println(user);
        }

        System.out.println("总记录数：" + page.getTotal());
        System.out.println("总页数:" + page.getPages());
        System.out.println("当前页数:" + page.getPageNum());
        System.out.println(page.isCount());

        while(++pageNum < page.getPages()){
            System.out.println("第"+pageNum+"页查询");
            PageHelper.startPage(pageNum, pageSize);
            page = (Page<User>)userMapper.selectUserList(param);
            for(User user:page.getResult()){
                System.out.println(user);
            }
        }

    }


    /**
     * 完整分页例子
     * 使用RowBounds的方式
     * @throws Exception
     */
    @Test
    public void run3() throws Exception{

        User param = new User();
        //param.setName("user2");

        int pageNum = 0;
        int pageSize= 10;
        System.out.println("第"+pageNum+"页查询");
        Page<User> page = (Page<User>)userMapper.selectUserListWithRowBounds(param , new RowBounds(pageNum ,pageSize));
        for(User user:page.getResult()){
            System.out.println(user);
        }

        System.out.println("总记录数：" + page.getTotal());
        System.out.println("总页数:" + page.getPages());
        System.out.println("当前页数:" + page.getPageNum());
        System.out.println(page.isCount());

        while(++pageNum < page.getPages()){
            System.out.println("第"+pageNum+"页查询");
            PageHelper.startPage(pageNum, pageSize);
            page = (Page<User>)userMapper.selectUserListWithRowBounds(param , new RowBounds(pageNum ,pageSize));
            for(User user:page.getResult()){
                System.out.println(user);
            }
        }
    }

}
