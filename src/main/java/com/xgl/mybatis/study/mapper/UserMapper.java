package com.xgl.mybatis.study.mapper;

import com.xgl.mybatis.study.model.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface UserMapper {

    List<User> selectUserList(User user);
    List<User> selectUserListForMap(Map<String ,Object> param);
    List<User> selectUserListWithRowBounds(User user , RowBounds rowBounds);
    List<User> selectUserListForParam(@Param("id") String id ,@Param("name") String name ,
                                      @Param("pageNum") int pageNum , @Param("pageSize") int pageSize);


    int insertUser(User user);
}
