package com.xgl.mybatis.study;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.xgl.mybatis.study.mapper.UserMapper;
import com.xgl.mybatis.study.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestPageHelper {

    private SqlSession sqlSession;
    private UserMapper userMapper;


    @Before
    public void before() throws Exception{
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();

        SqlSessionFactory sqlSessionFactory = builder.build(Resources.getResourceAsReader("mybatis-config.xml"));

        sqlSession = sqlSessionFactory.openSession(true);
        userMapper = sqlSession.getMapper(UserMapper.class);

    }


    @Test
    public void after() {
        sqlSession.commit();
        sqlSession.close();
    }




    /**
     * 使用Mybatis自带的RowBounds传递分页参数的方式
     * 注意：
     * 默认情况下，RowBounds是作为偏移量和页面条数进行查询的，如果我们习惯 页数 + 页码方式 进行分页时，需要设置分页插件的offsetAsPageNum参数为true
     * @throws Exception
     */
    @Test
    public void run1() throws Exception{

        List<User> userList = userMapper.selectUserListWithRowBounds( null ,new RowBounds( 0,10));
        for(User user: userList){
            System.out.println(user);
        }

        //如果使用RowBounds方式传递分页参数，那么需要配置rowBoundsWithCount=true才能得到以下返回参数
        Page page = (Page) userList;
        System.out.println("总记录数：" + page.getTotal());
        System.out.println("总页数："   + page.getPages());
        System.out.println("当前页："   + page.getPageNum());
        System.out.println("每页记录数：" + page.getPageSize());


        System.out.println("----------------------------------------------------------");

        //获取第二页
        userList = userMapper.selectUserListWithRowBounds( null ,new RowBounds( 2,10));

        //如果不设置分页插件的offsetAsPageNum参数为true，则查询为如下：
        //userList = userMapper.selectUserListWithRowBounds( null ,new RowBounds( 10,10));

        for(User user: userList){
            System.out.println(user);
        }





        System.out.println("----------------------------------------------------------");

        //获取第三页
        userList = userMapper.selectUserListWithRowBounds( null ,new RowBounds( 3,10));

        //如果不设置分页插件的offsetAsPageNum参数为true，则查询为如下：
        //userList = userMapper.selectUserListWithRowBounds( null ,new RowBounds( 20,10));

        for(User user: userList){
            System.out.println(user);
        }
    }







    /**
     * 使用PageHelper的startPage或offsetPage方式传递分页参数的方式
     * 说明：
     * 每次执行查询方法之前都要先调用PageHelper.startPage(pageNum ,pageSize)方法 ，
     * 然后紧着这调用查询方法。
     *
     * 注意：只有仅跟着PageHelper.startPage(pageNum ,pageSize)方法之后执行的sql查询，分页才生效
     *
     * @throws Exception
     */
    @Test
    public void run2() throws Exception{

        int pageNum = 1;
        int pageSize = 10;

        //获取第一页
        PageHelper.startPage(pageNum++ ,pageSize);  //每次都要执行
        //PageHelper.offsetPage(0 ,pageSize);       //每次都要执行 ,注意：这里第一个参数传入的不是页码而是偏移量=页码 * pageSize
        List<User> userList = userMapper.selectUserList( new User());
        for(User user: userList){
            System.out.println(user);
        }

        Page page = (Page) userList;
        System.out.println("总记录数：" + page.getTotal());
        System.out.println("总页数："   + page.getPages());
        System.out.println("当前页："   + page.getPageNum());
        System.out.println("每页记录数：" + page.getPageSize());




        System.out.println("----------------------------------------------------------");

        //获取第二页
        PageHelper.startPage(pageNum++,pageSize);  //每次都要执行
        //PageHelper.offsetPage(10,pageSize);      //每次都要执行 ,注意：这里第一个参数传入的不是页码而是偏移量=页码 * pageSize
        userList = userMapper.selectUserList( new User());

        for(User user: userList){
            System.out.println(user);
        }





        System.out.println("----------------------------------------------------------");

        //获取第三页
        //PageHelper.startPage(pageNum++ ,pageSize);  //每次都要执行
        PageHelper.offsetPage(20 ,pageSize);    //每次都要执行 ,注意：这里第一个参数传入的不是页码而是偏移量=页码 * pageSize
        userList = userMapper.selectUserList( new User());

        for(User user: userList){
            System.out.println(user);
        }
    }





    /**
     * 通过mapper接口方法参数方式传递分页参数
     * xxxxMethod(... ,@Param("pageNum") int pageNum , @Param("pageSize") int pageSize)
     * @throws Exception
     */
    @Test
    public void run3() throws Exception{

        int pageNum = 1;
        int pageSize = 10;

        //获取第一页
        List<User> userList = userMapper.selectUserListForParam( null ,null ,pageNum++ ,pageSize);
        for(User user: userList){
            System.out.println(user);
        }

        Page page = (Page) userList;
        System.out.println("总记录数：" + page.getTotal());
        System.out.println("总页数："   + page.getPages());
        System.out.println("当前页："   + page.getPageNum());
        System.out.println("每页记录数：" + page.getPageSize());




        System.out.println("----------------------------------------------------------");

        //获取第二页
        userList = userMapper.selectUserListForParam( null ,null ,pageNum++ ,pageSize);

        for(User user: userList){
            System.out.println(user);
        }





        System.out.println("----------------------------------------------------------");

        //获取第三页
        userList = userMapper.selectUserListForParam(null , null ,pageNum++ ,pageSize);

        for(User user: userList){
            System.out.println(user);
        }
    }




    /**
     * 通过Java POJO ，该POJO必须有pageSize和pageNum两个属性
     * @throws Exception
     */
    @Test
    public void run4() throws Exception{

        User param = new User();
        param.setPageSize(10);
        param.setPageNum(1);

        //获取第一页
        List<User> userList = userMapper.selectUserList(param);
        for(User user: userList){
            System.out.println(user);
        }

        Page page = (Page) userList;
        System.out.println("总记录数：" + page.getTotal());
        System.out.println("总页数："   + page.getPages());
        System.out.println("当前页："   + page.getPageNum());
        System.out.println("每页记录数：" + page.getPageSize());





        System.out.println("----------------------------------------------------------");

        //获取第二页
        param.setPageNum(2);
        userList = userMapper.selectUserList(param);

        for(User user: userList){
            System.out.println(user);
        }





        System.out.println("----------------------------------------------------------");

        //获取第三页
        param.setPageNum(3);
        userList = userMapper.selectUserList(param);

        for(User user: userList){
            System.out.println(user);
        }
    }




    /**
     * 通过Java Map ，该Map必须有pageSize和pageNum两个key
     * @throws Exception
     */
    @Test
    public void run5() throws Exception{

        Map<String ,Object> param = new HashMap<String ,Object>();
        param.put("pageNum"  ,1);
        param.put("pageSize" ,10);

        //获取第一页
        List<User> userList = userMapper.selectUserListForMap(param);
        for(User user: userList){
            System.out.println(user);
        }

        Page page = (Page) userList;
        System.out.println("总记录数：" + page.getTotal());
        System.out.println("总页数："   + page.getPages());
        System.out.println("当前页："   + page.getPageNum());
        System.out.println("每页记录数：" + page.getPageSize());





        System.out.println("----------------------------------------------------------");

        //获取第二页
        param.put("pageNum"  ,2);
        userList = userMapper.selectUserListForMap(param);

        for(User user: userList){
            System.out.println(user);
        }





        System.out.println("----------------------------------------------------------");

        //获取第三页
        param.put("pageNum"  ,3);
        userList = userMapper.selectUserListForMap(param);

        for(User user: userList){
            System.out.println(user);
        }
    }


}
