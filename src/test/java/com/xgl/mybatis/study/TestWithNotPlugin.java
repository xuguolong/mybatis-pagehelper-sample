package com.xgl.mybatis.study;


import com.xgl.mybatis.study.mapper.UserMapper;
import com.xgl.mybatis.study.model.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TestWithNotPlugin {

    private SqlSession sqlSession;
    private UserMapper userMapper;


    @Before
    public void before() throws Exception{
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();

        SqlSessionFactory sqlSessionFactory = builder.build(Resources.getResourceAsReader("mybatis-config.xml"));

        sqlSession = sqlSessionFactory.openSession(true);
        userMapper = sqlSession.getMapper(UserMapper.class);

    }


    @Test
    public void after() {
        sqlSession.commit();
        sqlSession.close();
    }


    /**
     * 使用Mybatis自带的分页功能
     */
    @Test
    public void page1() {
        RowBounds rowBounds = new RowBounds(0 ,10);
        List<User> users = userMapper.selectUserListWithRowBounds(null ,rowBounds);
        System.out.println(users);
        for(User user :users){
            System.out.println(user);
        }
    }





    @Test
    public void initUser(){
        for (int i = 7; i < 50; i++) {

            User user = new User();
            user.setName("user" + i);
            user.setAge(16+i);
            user.setSex(i/2);

            userMapper.insertUser(user);
        }
    }
}
